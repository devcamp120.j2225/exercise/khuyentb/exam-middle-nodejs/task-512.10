// Import thư viện express
const express = require('express');

// Import thư viện path
const path = require('path');

// Import router
const { router } = require('./Router/route');

// Tạo app
const app = new express();

// Tạo cổng chạy
const port = 8000;

// Sử dụng router
app.use('/', router);

// Khai báo API dạng GET "/" để chạy homepage
app.get('/', (req, res) => {
     console.log(__dirname);

     res.sendFile(path.join(__dirname + '/view/SignInForm.html'));
})



app.listen(port, () => {
     console.log(`App listening to port ${port}`);
})
